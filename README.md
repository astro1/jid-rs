jid-rs
======

What's this?
------------

A crate which provides a struct Jid for Jabber IDs. It's used in xmpp-rs but other XMPP libraries
can of course use this.

What license is it under?
-------------------------

LGPLv3 or later. See `COPYING` and `COPYING.LESSER`.

Notes
-----

This library does not yet implement RFC7622.

License yadda yadda.
--------------------

  Copyright 2017, jid-rs contributors.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
